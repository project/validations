function validationsProcess(fieldsDefinitions, formId){
	var jForm = $("#"+formId);

	jForm.submit(function (){
		var errorsFound = [];
		for(fieldName in fieldsDefinitions){
			if(fieldName.charAt(0) == "#"){
				// Omit this entry. It's not a field.
				continue;
			}


			var jField           = $("[name="+fieldName+"], [name="+fieldName+"]", jForm);
			var fieldValue       = jField.val();
			var fieldTitle       = fieldsDefinitions[fieldName]['#title'];
			var fieldValidations = fieldsDefinitions[fieldName]['#validations'];

			if( (jField.length > 0) && fieldValidations ){

				// See if this field is empty. It depends on its type.
				var isEmpty;
				switch( jField.attr("type") ){
					case "file":
					case "hidden":
					case "password":
					case "select":
					case "text":
						isEmpty = $.trim(jField.val()) == "";
						break;

					case "radio":
					case "radios":
					case "checkbox":
					case "checkboxes":
						// TODO: pending. All checkboxes are named differently like fieldname[fieldname1], fieldname[fieldname2], etc.
						// isEmpty = jField.filter(":checked").length == 0;
						break;

					default:
						if(jField.is("textarea")){
							isEmpty = jField.val() == "";
						} else {
							 // Default. What's this field anyway, if it's not any of the above?
							 isEmpty = true;
						}
						break;

				}

				var messageParameters = {
					"!field_title": fieldTitle,
					"%field_title": fieldTitle,
					"@field_title": fieldTitle
				};

				// Required
				if( validationValue = fieldValidations['required'] ){
					if(validationValue['value'] && isEmpty){
						errorsFound[fieldName] = Drupal.t(validationValue['message'], messageParameters);
					}
				}

				// Type
				var fieldType = "string"; // default value.
				if( validationValue = fieldValidations['type'] ){
					fieldType = validationValue['value'];

					switch(fieldType){
						case 'number':
						case 'float':
							if( !isEmpty && !parseFloat(fieldValue) ){
								errorsFound[fieldName] = Drupal.t(validationValue['message'], messageParameters);
							}
							break;

						case 'int':
							if( !isEmpty && !parseInt(fieldValue) ){
								errorsFound[fieldName] = Drupal.t(validationValue['message'], messageParameters);
							}
							break;

						case 'string':
							// This is the default value. There's nothing to validate.
							break;
					}
				}


			// Min
/*
			if( !$empty_field && isset($field_definition['#validations']['min']) ){
				$min = $field_definition['#validations']['min'];
				if( !is_numeric($min['value']) ){
					watchdog('validations', "Invalid minimum value %value in form %formid, field %field_title.", array('%value' => $min['value'], '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
				} else {
					switch($form[$field_name]['#type']){
						case 'select':
							if( !$form[$field_name]['#multiple'] ){
								watchdog('validations', "Minimum value %value required for a non-multiple select field in form %formid, field %field_title.", array('%value' => $min['value'], '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
								break;
							}

						case 'checkboxes':
							$selected = count(array_filter($field_value));
							if($selected < $min['value']){
								form_set_error( $field_name, t($min['message'], array('%field_title' => $field_title, '%value' => $min['value'])) );
							}
							break;

						case 'textfield':
						case 'textarea':
							switch($field_type){
								case 'number':
								case 'int':
								case 'float':
									if( $field_value < $min['value'] ){
										form_set_error( $field_name, t($min['message'], array('%field_title' => $field_title, '%value' => $min['value'])) );
									}
									break;

								case 'string':
								default:
									if( strlen($field_value) < $min['value'] ){
										form_set_error( $field_name, t($min['message'], array('%field_title' => $field_title, '%value' => $min['value'])) );
									}
									break;
							}
							break;

						default:
							watchdog('validations', "Minimum value %value required for unsupported field type %type in form %formid, field %field_title.", array('%value' => $min['value'], '%type' => $form[$field_name]['#type'], '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
							break;
					}
				}
			}
*/

			// Max
/*
			if( !$empty_field && isset($field_definition['#validations']['max']) ){
				$max = $field_definition['#validations']['max'];
				if( !is_numeric($max['value']) ){
					watchdog('validations', "Invalid maximum value %value in form %formid, field %field_title.", array('%value' => $max['value'], '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
				} else {
					switch($form[$field_name]['#type']){
						case 'select':
							if( !$form[$field_name]['#multiple'] ){
								watchdog('validations', "Maximum value %value required for a non-multiple select field in form %formid, field %field_title.", array('%value' => $max['value'], '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
								break;
							}
						case 'checkboxes':
							$selected = count(array_filter($field_value));
							if($selected > $max['value']){
								form_set_error( $field_name, t($max['message'], array('%field_title' => $field_title, '%value' => $max['value'])) );
							}
							break;

						case 'textfield':
						case 'textarea':
							switch($field_type){
								case 'number':
								case 'int':
								case 'float':
									if( $field_value > $max['value'] ){
										form_set_error( $field_name, t($max['message'], array('%field_title' => $field_title, '%value' => $max['value'])) );
									}
									break;

								case 'string':
								default:
									if( strlen($field_value) > $max['value'] ){
										form_set_error( $field_name, t($max['message'], array('%field_title' => $field_title, '%value' => $max['value'])) );
									}
									break;
							}
							break;

						default:
							watchdog('validations', "Maximum value %value required for unsupported field type %type in form %formid, field %field_title.", array('%value' => $max['value'], '%type' => $form[$field_name]['#type'], '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
							break;
					}
				}
			}
*/

			// String length
			if( validationValue = fieldValidations['length'] ){
				if( length = parseInt(validationValue['value']) ){
					if(!isEmpty && fieldValue.length != length){
						var parameters = $.extend(messageParameters, {
							'!value': length,
							'%value': length,
							'@value': length
						});

						errorsFound[fieldName] = Drupal.t(validationValue['message'], parameters);
					}
				}
			}

			// E-mail
			if( validationValue = fieldValidations['email'] ){
				var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

				if( !isEmpty && !emailRegex.test(fieldValue) ){
					errorsFound[fieldName] = Drupal.t(validationValue['message'], messageParameters);
				}
			}


/*
			// Regular expression
			if( !$empty_field && isset($field_definition['#validations']['regex']) ){
				$regex = $field_definition['#validations']['regex'];

				$patterns = $regex['value'];
				if( !is_array($patterns) ){
					$patterns = array($patterns);
				}
				$error_found = TRUE;
				foreach($patterns as $pattern){
					if( !preg_match($pattern, $field_value) ){
						$error_found = TRUE;
					}
				}

				if($error_found){
					$patterns = implode(',', $patterns);
					form_set_error( $field_name, t($regex['message'], array('%field_title' => $field_title, '%patterns' => $patterns, '%pattern' => $patterns)) );
				}
			}
*/

/*
			// Date
			if( !$empty_field && isset($field_definition['#validations']['date']) ){
				$date = $field_definition['#validations']['date'];

				// Interpret input, then transform again to string and compare.
				$time = strtotime($field_value);
				if( date($date['value'], $time) != trim($field_value) ){
					form_set_error( $field_name, t($date['message'], array('%field_title' => $field_title, '%dateformat' => $date['value'])) );
				}
			}
*/

/*
			// URL
			if( !$empty_field && isset($field_definition['#validations']['url']) ){
				$url = $field_definition['#validations']['url'];

				$URL_REGEX = "/^\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]$/i";
				if( $url['value'] && !preg_match($URL_REGEX, $field_value) ){
					form_set_error( $field_name, t($url['message'], array('%field_title' => $field_title)) );
				}
			}
*/

/*
			// Callback
			// Make an AJAX request onblur.
			if( !$empty_field && isset($field_definition['#validations']['callback']) ){
				$callback = $field_definition['#validations']['callback'];
				// Callback must have this arity: function callback($form, $form_state, $field_name)

				if( !function_exists($callback['value']) ){
					watchdog('validations', "Invalid callback function %value in form %formid, field %field_title.", array('%value' => $callback['value'], '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
				} else {
					$valid = $callback['value']($form, $form_state, $field_name);
					if( !$valid ){
						form_set_error( $field_name, t($callback['message'], array('%field_title' => $field_title)) );
					}
				}
			}
*/

			// Equal to other field (generally used for password fields.
			if( validationValue = fieldValidations['equal-to'] ){
				var otherFieldName = validationValue['value'];
				if( fieldValue != $("[name="+otherFieldName+"]").val() ){
					var otherFieldTitle = fieldsDefinitions[otherFieldName]['#title'];
					var parameters = $.extend(messageParameters, {
						'!other_field_title': otherFieldTitle,
						'%other_field_title': otherFieldTitle,
						'@other_field_title': otherFieldTitle
					});
					errorsFound[fieldName] = Drupal.t(validationValue['message'], parameters);
				}
			}

/*
			// Required if
			if( $empty_field && isset($field_definition['#validations']['required-if']) ){
				// 'required-if' => 'other_field_name=3', // supported =, ==, !=, <, >, <= and >=. Value can be empty.
				$required_if = $field_definition['#validations']['required-if'];

				$compare_to = preg_split("/([=(==)(!=)<(<=)>(>=)])/", $required_if['value'], 3, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_OFFSET_CAPTURE);
				$part_count = count($compare_to);

				if( $part_count < 1 || $part_count > 2 ){
					watchdog('validations', "Invalid required-if pattern %value in form %formid, field %field_title.", array('%value' => $required_if['value'], '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
				} else {
					$other_field_name        = $compare_to[0][0];
					$other_field_title       = $form[$other_field_name]['#title'];
					$other_field_name_length = strlen($other_field_name);

					if( isset($compare_to[1]) ){
						$other_field_value = $compare_to[1][0];
						$other_field_value_pos = $compare_to[1][1];
					} else {
						$other_field_value = '';
						$other_field_value_pos = strlen($required_if['value']);
					}

					$sign = substr($required_if['value'], $other_field_name_length, $other_field_value_pos - $other_field_name_length);

					switch($sign){
						case '=':
							$sign = '==';

						case '==':
						case '!=':
						case '<':
						case '<=':
						case '>':
						case '>=':
							eval('$invalid = $form_state["values"][$other_field_name] '.$sign.'$other_field_value;');
							if( $invalid ){
								form_set_error( $field_name, t($required_if['message'], array('%field_title' => $field_title, '%other_field_title' => $other_field_title)) );
							}
							break;

						default:
							watchdog('validations', "Invalid required-if sign %value in form %formid, field %field_title.", array('%value' => $sign, '%formid' => $form_id, '%field_title' => $field_name), WATCHDOG_WARNING);
							break;
					}
				}
			}
*/
			}
		} // for(fieldName in fieldsDefinitions)




		// Show errors, if any.
		// Only method supported now is div before form.
		var errorMessages = [];
		for(fieldName in errorsFound){
			errorMessages.push(errorsFound[fieldName]);
		}

		if(errorMessages.length > 0){
			var messages = "";
			if(errorMessages.length > 1){
				// Show in UL.
				messages = "<ul><li>"+errorMessages.join("</li><li>")+"</li></ul>";
			} else {
				// Single message.
				messages = errorMessages.join("");
			}

			var errorsDivId = formId+"_validations";
			var errorsDiv = $("div#"+errorsDivId);
			if(errorsDiv.length == 0){
				errorsDiv = $("<div>")
					.addClass("messages")
					.addClass("error")
					.attr("id", errorsDivId)
					.insertBefore("form#"+formId)
				;
			}

			errorsDiv
				.html(messages)
				.stop().fadeOut(500).fadeIn(500)
			;
		}

		return errorMessages.length == 0;
	}); // jForm.submit
}
