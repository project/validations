Examples of possible validations

$form['field'] = array(
	'#type' => 'textfield',

	'#title' => t("A test fied"),

	'#validations' = array(
		'#fill-description' => TRUE,
		'required' => TRUE | FALSE,
		'required' => array(
			'value' => TRUE,
			'message' => 'Please enter something in %field_title',
		),
		'type' => number, int, float, string (default)
		'email' => TRUE | FALSE,
		'min' => 0, // for checkboxes or multiple select, min number selected
		'max' => 255,
		'url' => TRUE | FALSE,
		'regex' => "/^.*$/i",
		'regex' => array("/^.*$/i", "/^.*$/i"),
		'date' => 'Y-m-d',
		'callback' => 'callback_function', // arity ($form, $form_state, $field_name)
		'equal-to' => 'other_field_name', // useful for password + confirm password
		'required-if' => 'other_field!=3', // supported =, ==, !=, <, >, <= and >=. Value can be empty.
		'url' => TRUE,
	),
);

