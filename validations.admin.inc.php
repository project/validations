<?php

function validations_settings_form(){
	$form = array();

	$settings = validations_settings();

	$form['default_messages'] = array(
		'#type' => 'fieldset',
		'#title' => t("Default error messages"),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#description' => t("Here you can define your custom default error messages. These are parsed through t(), so translations can take place. You'll see that for each message there are some available placeholders you can include; they will be replaced automatically with the right values before showing the message."),
	);

	$form['default_messages']['message-required'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Required field</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-required'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title'))
	);

	$form['default_messages']['message-type-float'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Float required</em> message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-type-float'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title'))
	);

	$form['default_messages']['message-type-int'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Integer required</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-type-int'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title'))
	);

	$form['default_messages']['message-min-number'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Minimum number</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-min-number'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %value'))
	);

	$form['default_messages']['message-min-string'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Minimum string length</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-min-string'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %value'))
	);

	$form['default_messages']['message-min-options'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Minimum options selected</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-min-options'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %value'))
	);

	$form['default_messages']['message-max-number'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Maximum number</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-max-number'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %value'))
	);

	$form['default_messages']['message-max-string'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Maximum string length</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-max-string'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %value'))
	);

	$form['default_messages']['message-max-options'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Maximum options selected</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-max-options'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %value'))
	);

	$form['default_messages']['message-string-length'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>String length</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-string-length'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %value'))
	);

	$form['default_messages']['message-email'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>E-mail</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-email'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title'))
	);

	$form['default_messages']['message-date'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Date</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-date'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %dateformat'))
	);

	$form['default_messages']['message-url'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>URL</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-url'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title'))
	);

	$form['default_messages']['message-regex'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>1 regular expression</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-regex'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %pattern'))
	);

	$form['default_messages']['message-callback'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Callback</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-callback'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title'))
	);

	$form['default_messages']['message-equal-to'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Equal to field</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-equal-to'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %other_field_title'))
	);

	$form['default_messages']['message-required-if'] = array(
		'#type' => 'textfield',
		'#title' => t("<em>Required if</em> error message"),
		'#size' => 77,
		'#maxlength' => 255,
		'#required' => TRUE,
		'#default_value' => $settings['message-required-if'],
		'#description' => t("Available placeholders: %placeholders.", array('%placeholders' => '%field_title, %other_field_title'))
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save changes',
	);

	return $form;
}

function validations_settings_form_validate($form, &$form_state){
	// Nothing to validate... for the time being, anyway.
}

function validations_settings_form_submit($form, &$form_state){
	$current_settings = validations_settings();

	$new_settings = array(
		'message-callback'      => $form_state['values']['message-callback'],
		'message-date'          => $form_state['values']['message-date'],
		'message-email'         => $form_state['values']['message-email'],
		'message-equal-to'      => $form_state['values']['message-equal-to'],
		'message-max-number'    => $form_state['values']['message-max-number'],
		'message-max-string'    => $form_state['values']['message-max-string'],
		'message-min-number'    => $form_state['values']['message-min-number'],
		'message-min-string'    => $form_state['values']['message-min-string'],
		'message-regex'         => $form_state['values']['message-regex'],
		'message-regexes'       => $form_state['values']['message-regexes'],
		'message-required'      => $form_state['values']['message-required'],
		'message-required-if'   => $form_state['values']['message-required-if'],
		'message-string-length' => $form_state['values']['message-string-length'],
		'message-type-float'    => $form_state['values']['message-type-float'],
		'message-type-int'      => $form_state['values']['message-type-int'],
		'message-url'           => $form_state['values']['message-url'],
		'message-min-options'   => $form_state['values']['message-min-options'],
		'message-max-options'   => $form_state['values']['message-max-options'],

	);

	$new_settings = array_merge($current_settings, $new_settings);

	variable_set('validations_settings', $new_settings);

	drupal_set_message(t("Validation messages saved."));
}
